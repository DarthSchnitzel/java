/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package urcovac;

/**
 *
 * @author Martin
 */
public class Zapasnik {
    private String meno;
    private int HP ;
    private int MAX_HP;
    private int utok;
    private int obrana;
    private Kocka kocka;
    
    private String sprava;
    
    public Zapasnik (String meno, int HP, int utok, int obrana, urcovac.Kocka kocka) {
        this.meno = meno ;
        this.HP = HP;
        this.MAX_HP = HP;
        this.utok = utok;
        this.obrana= obrana;
        this.kocka = kocka;
    }
        
    public boolean zije () {
        return (HP > 0);
    }
    
    public String HP_line () {
        String s = "[";
        int celkom = 20 ;
        double pocet = Math.round(((double)HP/MAX_HP) * celkom);
        
        if ((pocet == 0) && (zije())) {
            pocet = 1;
        }
        
        for (int i = 0; i < pocet; i++) {
            s += "#";
        }
        for (int i = 0; i < celkom - pocet; i++) {
            s += " ";
        }
        s += "]";
        return s; 
    }

    public void Uhyb (int rana) {
        int zranenie = rana  - (obrana + kocka.Tah());
        
        System.out.printf("rana %s , obrana %s ",rana,obrana);
        if (zranenie > 0) {
            sprava = String.format("%s schytáva ranu za %s HP", meno,zranenie);
            HP -= zranenie;
            if (HP <= 0 ) {
                HP = 0;
            }
        } else {
            sprava=String.format("PRD S MASLOM. %s sa uhýba :P", meno);
        }
        NastavSpravu(sprava);
    }
    
    public void Zautoc (Zapasnik protivnik){
        int uder = utok + kocka.Tah();
        NastavSpravu(String.format("%s striela za %s HP", meno,uder));
        protivnik.Uhyb(uder);
    }
    
    @Override
    public String toString () {
        return meno;
    }
    
    private void NastavSpravu (String sprava) {
        this.sprava = sprava;
    }

    public String VratPoslednuSpravu () {
        return sprava;
    }

}
