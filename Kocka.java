package urcovac;

/*
* zaklady hracej kocky 
* TODO: grafika, ovládanie na tlačítko
 */
import java.util.Random;

public class Kocka {
    
    private Random nahoda;
    private int pocet_hran;
    
    public Kocka() {
        pocet_hran= 6;
        nahoda = new Random();
    };
    
    public int Tah() {
        return nahoda.nextInt(pocet_hran);
    };
    
    public int VratPocetHran() {
        return pocet_hran;
    }
    
    @Override
    public String toString() {
        return String.format("Kocka s %s hranamy", pocet_hran);
    }
}
